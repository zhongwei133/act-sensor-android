function getAllArchive() {
    const url = "/archive/getAllUserArchive";
    $.ajax({
        type: "get",
        url: url,
        success: function (data) {
            console.log(data)
        }
    });
}

function getAllSensorData() {
    const url = "/data/getAllData";
    $.ajax({
        type: "get",
        url: url,
        success: function (data) {
            console.log(data)
        }
    });
}
function getAllUsers() {
    const url = "/base/getAllUsers";
    $.ajax({
        type: "get",
        url: url,
        success: function (data) {
            console.log(data)
        }
    });
}
function getOneRecord() {
    const url = "/whole/getRecordIdToUse";
    $.ajax({
        type: "get",
        url: url,
        success: function (data) {
            console.log(data)
        }
    });
}
function sendOneGroup() {
    const url = "/whole/sendData";
    $.ajax({
        type: "post",
        url: url,
        data:{'s1':$("#s1").val(),
            's2' : $("#s2").val(),
            's3' : $("#s3").val(),
            's4' : $("#s4").val(),
            's5' : $("#s5").val(),
            's6' : $("#s6").val(),
            's7' : $("#s7").val(),
            's8' : $("#s8").val(),
            's9' : $("#s9").val(),
            's10' : $("#s10").val(),
            'userSerial':$("#userSerial").val(),
            'recordID':$("#recordID").val()
        },
        success: function (data) {
            console.log(data)
        }
    });
}
function doEnd() {
    const url = "/whole/endSleep";
    $.ajax({
        type: "get",
        url: url,
        data:{
            'userSerial':$("#userSerial1").val(),
            'recordID':$("#recordID1").val()
        },
        success: function (data) {
            console.log(data)
        }
    });
}
function doAsk() {
    const url = "/whole/wakeup";
    $.ajax({
        type: "get",
        url: url,
        data:{
            'userSerial':$("#userSerial2").val(),
            'recordID':$("#recordID2").val()
        },
        success: function (data) {
            console.log(data)
        }
    });
}


