package com.kermit.blinderserverv1.view;

public class SensorDataV {
    private Float s1;
    private Float s2;
    private Float s3;
    private Float s4;
    private Float s5;
    private Float s6;
    private Float s7;
    private Float s8;
    private Float s9;
    private Float s10;
    private Integer recordID;
    private Integer userSerial;

    public Float getS1() {
        return s1;
    }

    public void setS1(Float s1) {
        this.s1 = s1;
    }

    public Float getS2() {
        return s2;
    }

    public void setS2(Float s2) {
        this.s2 = s2;
    }

    public Float getS3() {
        return s3;
    }

    public void setS3(Float s3) {
        this.s3 = s3;
    }

    public Float getS4() {
        return s4;
    }

    public void setS4(Float s4) {
        this.s4 = s4;
    }

    public Float getS5() {
        return s5;
    }

    public void setS5(Float s5) {
        this.s5 = s5;
    }

    public Float getS6() {
        return s6;
    }

    public void setS6(Float s6) {
        this.s6 = s6;
    }

    public Float getS7() {
        return s7;
    }

    public void setS7(Float s7) {
        this.s7 = s7;
    }

    public Float getS8() {
        return s8;
    }

    public void setS8(Float s8) {
        this.s8 = s8;
    }

    public Float getS9() {
        return s9;
    }

    public void setS9(Float s9) {
        this.s9 = s9;
    }

    public Float getS10() {
        return s10;
    }

    public void setS10(Float s10) {
        this.s10 = s10;
    }

    public Integer getRecordID() {
        return recordID;
    }

    public void setRecordID(Integer recordID) {
        this.recordID = recordID;
    }

    public Integer getUserSerial() {
        return userSerial;
    }

    public void setUserSerial(Integer userSerial) {
        this.userSerial = userSerial;
    }
}
