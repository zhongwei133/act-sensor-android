package com.kermit.blinderserverv1.action;

import com.kermit.blinderserverv1.bean.UserSensorData;
import com.kermit.blinderserverv1.service.UserSensorDataService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/data")
public class UserSensorDataAction {
    @Resource
    private UserSensorDataService userSensorDataService;

    @RequestMapping("/getAllData")
    public List<UserSensorData> getAllSensorData()
    {
        return userSensorDataService.getAllUserSensorData();
    }

    public Boolean insertDataInfo()
    {
        UserSensorData userSensorData = new UserSensorData();
        return userSensorDataService.insert(userSensorData);
    }
}
