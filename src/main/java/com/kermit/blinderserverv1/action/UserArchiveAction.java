package com.kermit.blinderserverv1.action;

import com.kermit.blinderserverv1.bean.UserArchive;
import com.kermit.blinderserverv1.service.UserArchiveService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/archive")
public class UserArchiveAction {
    @Resource
    private UserArchiveService userArchiveService;

    @RequestMapping(value = "/getAllUserArchive",method = RequestMethod.GET)
    public List<UserArchive> getAllUserArchive(){
        return userArchiveService.getAllUserArchive();
    }

    @RequestMapping(value = "/getRecordIdToUse")
    public Integer getRecordIdToUse() {
        return userArchiveService.getRecordId();
    }
//    @RequestMapping(value = "/insertPro")
//    public Boolean insertPro(Integer userSerial, Integer recordID) throws ParseException {
//        return userArchiveService.insertPro(userSerial,recordID);
//    }
    @RequestMapping(value = "/insertRear")
    public Boolean insertRear(Integer userSerial, Integer recordID)
    {
        return userArchiveService.insertRear(userSerial,recordID);
    }

}
