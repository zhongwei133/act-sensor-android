package com.kermit.blinderserverv1.action;

import com.kermit.blinderserverv1.service.UserArchiveService;
import com.kermit.blinderserverv1.service.impl.CalDataServiceImpl;
import com.kermit.blinderserverv1.view.SensorDataV;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/whole")
public class UserWholeTaskAction {
    @Resource
    private CalDataServiceImpl calDataService;

    @Resource
    private UserArchiveService userArchiveService;

    @RequestMapping("/sendData")
    public Boolean sendNormalData(Float s1,Float s2,Float s3,Float s4,Float s5,Float s6,Float s7,Float s8,Float s9,Float s10,Integer recordID,Integer userSerial)
    {
        SensorDataV sensorDataV = new SensorDataV();
        sensorDataV.setS1(s1);
        sensorDataV.setS2(s2);
        sensorDataV.setS3(s3);
        sensorDataV.setS4(s4);
        sensorDataV.setS5(s5);
        sensorDataV.setS6(s6);
        sensorDataV.setS7(s7);
        sensorDataV.setS8(s8);
        sensorDataV.setS9(s9);
        sensorDataV.setS10(s10);
        sensorDataV.setRecordID(recordID);
        sensorDataV.setUserSerial(userSerial);
        return calDataService.calTendata(sensorDataV);
    }

    @GetMapping(value = "/getRecordIdToUse")
    public Integer getRecordIdToUse() {
        return userArchiveService.getRecordId();
    }

    @GetMapping(value = "/endSleep")
    public Boolean insertRear(Integer userSerial, Integer recordID)
    {
        return userArchiveService.insertRear(userSerial,recordID);
    }
    @GetMapping(value = "/wakeup")
    public Boolean wakeup(Integer recordID)
    {
        return calDataService.dowake(recordID);
    }


}
