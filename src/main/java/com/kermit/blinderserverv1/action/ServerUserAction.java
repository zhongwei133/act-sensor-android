package com.kermit.blinderserverv1.action;

import com.kermit.blinderserverv1.bean.UserProfile;
import com.kermit.blinderserverv1.service.ServerUserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/base")
public class ServerUserAction {
    @Resource
    private ServerUserService serverUserService;

    @RequestMapping(value = "/getAllUsers")
    public List<UserProfile> getAllUsers()
    {
        return serverUserService.getAllUserProfile();
    }
    @RequestMapping(value = "/getAllUserProfile",method = RequestMethod.GET)
    public String getAllUserProfile(){
        ArrayList<UserProfile> userProfiles = (ArrayList<UserProfile>) serverUserService.getAllUserProfile();
        String res = "";
        for (UserProfile cur:userProfiles) {
            res += cur.toString() + "<br>";
        }
        return res;
    }

    @RequestMapping(value = "/login")
    public String userLogin(@RequestParam("userId")     String userId,
                            @RequestParam("password")   String password){

        return serverUserService.login(userId, password);
    }
}
