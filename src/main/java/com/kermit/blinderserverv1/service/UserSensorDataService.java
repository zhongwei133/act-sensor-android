package com.kermit.blinderserverv1.service;

import com.kermit.blinderserverv1.bean.UserSensorData;

import java.util.List;

public interface UserSensorDataService {
    public List<UserSensorData> getAllUserSensorData();
    public Boolean insert(UserSensorData userSensorData);
}
