package com.kermit.blinderserverv1.service;

import com.kermit.blinderserverv1.bean.UserArchive;

import java.text.ParseException;
import java.util.List;

public interface UserArchiveService {
    public List<UserArchive> getAllUserArchive();
    public Integer getRecordId();
    public Boolean insertPro(UserArchive userArchive);
    public Boolean insertRear(Integer userSerial,Integer recordID);
}
