package com.kermit.blinderserverv1.service.impl;

import com.kermit.blinderserverv1.bean.UserArchive;
import com.kermit.blinderserverv1.bean.UserSensorData;
import com.kermit.blinderserverv1.service.CalDataService;
import com.kermit.blinderserverv1.service.UserArchiveService;
import com.kermit.blinderserverv1.service.UserSensorDataService;
import com.kermit.blinderserverv1.utils.AlgorithmCal;
import com.kermit.blinderserverv1.view.SensorDataV;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

import static com.kermit.blinderserverv1.utils.DivideSensorData.ToBeTwoDataSet;

@Service("calDataService")
public class CalDataServiceImpl implements CalDataService {
    @Resource
    private UserArchiveService userArchiveService;

    @Resource
    private UserSensorDataService userSensorDataService;

    @Override
    public Boolean calTendata(SensorDataV sensorDataV) {
        try{
            Map map = ToBeTwoDataSet(sensorDataV);

            UserArchive userArchive = (UserArchive) map.get("userArchive");
            userArchiveService.insertPro(userArchive);//档案记录表中插入一半

            UserSensorData userSensorData =(UserSensorData) map.get("userSensorData");
            userSensorDataService.insert(userSensorData);//向传感器数据记录表中插入数据

            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Boolean dowake(Integer recordID) {
        return AlgorithmCal.IsShouldBeWake(recordID);
    }
}
