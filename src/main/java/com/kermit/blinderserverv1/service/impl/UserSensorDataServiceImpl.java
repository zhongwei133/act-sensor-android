package com.kermit.blinderserverv1.service.impl;

import com.kermit.blinderserverv1.bean.UserSensorData;
import com.kermit.blinderserverv1.mapper.UserSensorDataMapper;
import com.kermit.blinderserverv1.service.UserSensorDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userSensorDataService")
public class UserSensorDataServiceImpl implements UserSensorDataService {
    @Resource
    private UserSensorDataMapper userSensorDataMapper;

    @Override
    public List<UserSensorData> getAllUserSensorData() {
        return userSensorDataMapper.getAllUserSensorData();
    }

    @Override
    public Boolean insert(UserSensorData userSensorData) {
        return userSensorDataMapper.insert(userSensorData);
    }
}
