package com.kermit.blinderserverv1.service.impl;

import com.kermit.blinderserverv1.bean.UserArchive;
import com.kermit.blinderserverv1.mapper.UserArchiveMapper;
import com.kermit.blinderserverv1.service.UserArchiveService;
import com.kermit.blinderserverv1.utils.AlgorithmCal;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service("userArchiveService")
public class UserArchiveServiceImpl implements UserArchiveService {
    @Resource
    private UserArchiveMapper userArchiveMapper;

    @Override
    public List<UserArchive> getAllUserArchive() {
        return userArchiveMapper.getAllUserArchive();
    }

    @Override
    public Integer getRecordId() {
        return userArchiveMapper.getRecordId()+1;
    }

    @Override
    public Boolean insertPro(UserArchive userArchive){
        try{
            List<UserArchive> list = userArchiveMapper.querIsExist(userArchive);
            if(list.size()==0)//没有才插入
            {
                userArchive.setStartTime(new Date());
                userArchiveMapper.insertPro(userArchive);
                return true;
            }
            return false;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Boolean insertRear(Integer userSerial, Integer recordID) {
        try{

            UserArchive userArchive = new UserArchive();

            userArchive.setUserSerial(userSerial);//设置用户ID
            userArchive.setRecordID(recordID);//设置记录ID

            userArchive.setEndTime(new Date()); //设置当前时间

            //机器学习算法调用
            //睡眠质量打分
            userArchive.setSleepQuality(AlgorithmCal.GetSleepQuality(userArchive.getRecordID(),userArchive.getStartTime(),userArchive.getEndTime()));//睡眠质量评分算法
            //翻身次数统计
            userArchive.setTurnoverTimes(AlgorithmCal.GetTurnOverNum(userArchive.getRecordID()));//翻身次数算法调用
            //生成提高睡眠质量建议
            AlgorithmCal.giveAdvice(userArchive.getRecordID());

            userArchiveMapper.insertRear(userArchive);
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
