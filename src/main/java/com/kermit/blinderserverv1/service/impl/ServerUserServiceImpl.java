package com.kermit.blinderserverv1.service.impl;

import com.kermit.blinderserverv1.bean.UserProfile;
import com.kermit.blinderserverv1.mapper.ServerUserMapper;
import com.kermit.blinderserverv1.service.ServerUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service("serverUserService")
public class ServerUserServiceImpl implements ServerUserService {

    @Resource
    private ServerUserMapper serverUserMapper;

    @Override
    public String login(String userID, String password) {
        ArrayList<UserProfile> userProfiles = (ArrayList<UserProfile>) serverUserMapper.login(userID,password);
        if (userProfiles.size() == 0){
            return "null user";
        }
        else {
            return userProfiles.get(0).getUserName()+","+userProfiles.get(0).getUserSerial();
        }
    }

    /*@Override
    public String getUserName(String userID) {
        return null;
    }*/

    @Override
    public List<UserProfile> getAllUserProfile() {
        return serverUserMapper.getAllUserProfile();
    }
}
