package com.kermit.blinderserverv1.service;

import com.kermit.blinderserverv1.bean.UserProfile;

import java.util.List;

public interface ServerUserService {
    public String login(String userID,String password);

//    public String getUserName(String userID);

    public List<UserProfile> getAllUserProfile();
}
