package com.kermit.blinderserverv1.service;

import com.kermit.blinderserverv1.view.SensorDataV;

public interface CalDataService {
    public Boolean calTendata(SensorDataV sensorDataV);
    public Boolean dowake(Integer recordID);
}
