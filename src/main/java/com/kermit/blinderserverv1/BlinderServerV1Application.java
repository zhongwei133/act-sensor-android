package com.kermit.blinderserverv1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("com.kermit.blinderserverv1.mapper")
@SpringBootApplication
public class BlinderServerV1Application {

    public static void main(String[] args) {
        SpringApplication.run(BlinderServerV1Application.class, args);
    }

}
