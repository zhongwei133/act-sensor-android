package com.kermit.blinderserverv1.mapper;

import com.kermit.blinderserverv1.bean.UserArchive;
import com.kermit.blinderserverv1.bean.UserSensorData;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserSensorDataMapper {

    @Select({
            "select",
            "*",
            "from sensordatatb"
    })
    public List<UserSensorData> getAllUserSensorData();

    @Insert({
            "insert into sensordatatb",
            "(sensor1,sensor2,sensor3,sensor4,sensor5,sensor6,sensor7,sensor8,sensor9,timeStamps,recordID)",
            "values",
            "(#{sensor1},#{sensor2},#{sensor3},#{sensor4},#{sensor5},#{sensor6},#{sensor7},#{sensor8},#{sensor9},#{timeStamps},#{recordID})"
    })
    public Boolean insert(UserSensorData userSensorData);

}
