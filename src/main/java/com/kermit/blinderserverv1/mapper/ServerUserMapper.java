package com.kermit.blinderserverv1.mapper;

import com.kermit.blinderserverv1.bean.UserProfile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ServerUserMapper {

    @Select({
            "select",
            "*",
            "from userprofiletb;"
    })
    public List<UserProfile> getAllUserProfile();

    @Select({
            "select",
            "*",
            "from userprofiletb",
            "where userID = #{userID} and userpwd= #{password}"
    })
    public List<UserProfile> login(String userID,String password);

    /*@Select({
            "select",
            "userName",
            "from userprofiletb",
            "where userID = #{userID}"
    })
    public String getUserName(String userID);*/
}
