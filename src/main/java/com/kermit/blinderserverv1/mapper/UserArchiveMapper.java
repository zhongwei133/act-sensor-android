package com.kermit.blinderserverv1.mapper;

import com.kermit.blinderserverv1.bean.UserArchive;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserArchiveMapper {

    @Select({
            "select",
            "*",
            "from archivetb"
    })
    public List<UserArchive> getAllUserArchive();

    @Select({
            "select",
            "max(recordID)",
            "from archivetb"
    })
    public Integer getRecordId();

    @Select({
            "select",
            "*",
            "from archivetb",
            "where userSerial=#{userSerial} and recordID=#{recordID}"
    })
    public List<UserArchive> querIsExist(UserArchive userArchive);

    @Insert({
            "insert into archivetb",
            "(userSerial,recordID,startTime)",
            "values",
            "(#{userSerial},#{recordID},#{startTime})"
    })
    public Boolean insertPro(UserArchive userArchive);
//UPDATE `blinder`.`archivetb` SET `endTime` = '2021-01-05 09:12:56', `sleepQuality` = '86', `turnoverTimes` = '57' WHERE (`archiveSerial` = '10');
    @Update({
            "update archivetb",
            "set endTime=#{endTime}",
            ",sleepQuality=#{sleepQuality}",
            ",turnoverTimes=#{turnoverTimes}",
            "where userSerial=#{userSerial} and recordID=#{recordID}"
    })
    public Boolean insertRear(UserArchive userArchive);
}
