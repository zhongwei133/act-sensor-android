package com.kermit.blinderserverv1.utils;

import com.kermit.blinderserverv1.bean.UserArchive;
import com.kermit.blinderserverv1.bean.UserSensorData;
import com.kermit.blinderserverv1.view.SensorDataV;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DivideSensorData {
    public static Map ToBeTwoDataSet(SensorDataV sensorDataV)
    {
        UserArchive userArchive = new UserArchive();

        userArchive.setUserSerial(sensorDataV.getUserSerial());
        userArchive.setRecordID(sensorDataV.getRecordID());

        UserSensorData userSensorData = new UserSensorData();

        userSensorData.setSensor1(sensorDataV.getS2());//x
        userSensorData.setSensor2(sensorDataV.getS3());//y
        userSensorData.setSensor3(sensorDataV.getS4());//z
        userSensorData.setSensor4(sensorDataV.getS5());//气压
        userSensorData.setSensor5(sensorDataV.getS6());//环境温度
        userSensorData.setSensor6(sensorDataV.getS7());//湿度
        userSensorData.setSensor7(sensorDataV.getS8());//心率
        userSensorData.setSensor8(sensorDataV.getS9());//血氧
        userSensorData.setSensor9(sensorDataV.getS10());//体温
        userSensorData.setRecordID(sensorDataV.getRecordID());//记录号

        userSensorData.setTimeStamps(new Date());//当前时间戳

        Map map = new HashMap();
        map.put("userArchive",userArchive);
        map.put("userSensorData",userSensorData);
        return map;
    }
}
