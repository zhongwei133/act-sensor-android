package com.kermit.blinderserverv1.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class AlgorithmCal {
    private static Integer sleepScore;
    private static Integer turnoverNum;
    private static Boolean WakeFlag;

    public static Integer GetSleepQuality(Integer recordID, Date startTime,Date endTime)
    {
        Process process;
        //调用python算法给睡眠质量评分的接口
        try {
            String[] args = new String[] { "python", "D:\\GradeThreeSecond\\hope\\code\\Algorithm2.0\\get_score.py", String.valueOf(recordID),"2021-01-01 00:00:00","2021-01-01 00:00:11"};

            process = Runtime.getRuntime().exec(args);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                int idx = line.lastIndexOf(".");//查找小数点的位置
                if(idx==-1)
                    sleepScore = Integer.valueOf(line);
                else
                    sleepScore = Integer.valueOf(line.substring(0,idx));
                //System.out.println(line);
            }
            in.close();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return sleepScore;
    }
    public static Integer GetTurnOverNum(Integer recordID)
    {
        //调用python算法给翻身次数的接口
        Process process;
        try {
            String[] args = new String[] { "python", "D:\\GradeThreeSecond\\hope\\code\\Algorithm\\turnoveNumCal.py", String.valueOf(recordID),"2021-01-01 00:00:00","2021-01-01 00:00:11"};

            process = Runtime.getRuntime().exec(args);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                //System.out.println(line);
                turnoverNum = Integer.valueOf(line);
            }
            in.close();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return turnoverNum;
    }
    public static void giveAdvice(Integer recordID)
    {
        Process process;
        //调用python算法更新数据库建议表
        try {
            String[] args = new String[] { "python", "D:\\GradeThreeSecond\\hope\\code\\Algorithm\\advice_gen.py", String.valueOf(recordID),"2021-01-01 00:00:00","2021-01-01 00:00:11"};

            process = Runtime.getRuntime().exec(args);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
            in.close();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static Boolean IsShouldBeWake(Integer recordID)
    {
        Process process;
        //调用python算法给出是否唤醒的决策
        try {
            String[] args = new String[] { "python", "D:\\GradeThreeSecond\\hope\\code\\Algorithm\\determine_wakeup.py", String.valueOf(recordID),"2021-01-01 00:00:11"};

            process = Runtime.getRuntime().exec(args);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                WakeFlag = Boolean.parseBoolean(line);
                //System.out.println(line);
            }
            in.close();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return WakeFlag;
    }
}
