package com.kermit.blinderserverv1.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class UserArchive {
    private Integer archiveSerial;
    private Integer userSerial;
    private Integer recordID;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date startTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date endTime;

    private Integer sleepQuality;
    private Integer turnoverTimes;

    public Integer getArchiveSerial() {
        return archiveSerial;
    }

    public void setArchiveSerial(Integer archiveSerial) {
        this.archiveSerial = archiveSerial;
    }

    public Integer getUserSerial() {
        return userSerial;
    }

    public void setUserSerial(Integer userSerial) {
        this.userSerial = userSerial;
    }

    public Integer getRecordID() {
        return recordID;
    }

    public void setRecordID(Integer recordID) {
        this.recordID = recordID;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getSleepQuality() {
        return sleepQuality;
    }

    public void setSleepQuality(Integer sleepQuality) {
        this.sleepQuality = sleepQuality;
    }

    public Integer getTurnoverTimes() {
        return turnoverTimes;
    }

    public void setTurnoverTimes(Integer turnoverTimes) {
        this.turnoverTimes = turnoverTimes;
    }
}
