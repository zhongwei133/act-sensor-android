package com.kermit.blinderserverv1.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class UserSensorData {
    private Integer dataLineID;
    private Float sensor1;
    private Float sensor2;
    private Float sensor3;
    private Float sensor4;
    private Float sensor5;
    private Float sensor6;
    private Float sensor7;
    private Float sensor8;
    private Float sensor9;
    private Integer recordID;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date timeStamps;

    public Integer getDataLineID() {
        return dataLineID;
    }

    public void setDataLineID(Integer dataLineID) {
        this.dataLineID = dataLineID;
    }

    public Float getSensor1() {
        return sensor1;
    }

    public void setSensor1(Float sensor1) {
        this.sensor1 = sensor1;
    }

    public Float getSensor2() {
        return sensor2;
    }

    public void setSensor2(Float sensor2) {
        this.sensor2 = sensor2;
    }

    public Float getSensor3() {
        return sensor3;
    }

    public void setSensor3(Float sensor3) {
        this.sensor3 = sensor3;
    }
    public Float getSensor4() {
        return sensor4;
    }

    public void setSensor4(Float sensor4) {
        this.sensor4 = sensor4;
    }

    public Float getSensor5() {
        return sensor5;
    }

    public void setSensor5(Float sensor5) {
        this.sensor5 = sensor5;
    }

    public Float getSensor6() {
        return sensor6;
    }

    public void setSensor6(Float sensor6) {
        this.sensor6 = sensor6;
    }

    public Float getSensor7() {
        return sensor7;
    }

    public void setSensor7(Float sensor7) {
        this.sensor7 = sensor7;
    }

    public Float getSensor8() {
        return sensor8;
    }

    public void setSensor8(Float sensor8) {
        this.sensor8 = sensor8;
    }

    public Float getSensor9() {
        return sensor9;
    }

    public void setSensor9(Float sensor9) {
        this.sensor9 = sensor9;
    }

    public Date getTimeStamps() {
        return timeStamps;
    }

    public void setTimeStamps(Date timeStamps) {
        this.timeStamps = timeStamps;
    }

    public Integer getRecordID() {
        return recordID;
    }

    public void setRecordID(Integer recordID) {
        this.recordID = recordID;
    }
}
